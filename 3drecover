#! /usr/bin/env ruby

# Change these file locations to match your needs
# Personally, I have an NTFS share on my Octopi server so when I'm running this on my desktop, it's running on files on the actual pi.
$source = "/home/keith/pi/uploads/"
$destination = "/home/keith/pi/recoveries/"
$layer = ARGV[0]

unless ARGV[0]
	puts "Pass the recover layer, and optionally filename. If you don't pass a filename it will use the last uploaded file."
	puts "e.g. 3drecover 20"
end 

def last_upload 
	files = Dir.entries($source)
	return files.last
end

def analyse_file file
	puts "ANALYZING GCODE"
	text=File.open($source + file).read
	text.gsub!(/\r\n?/, "\n")
	$phase = "start"
	text.each_line do |line|
	  case $phase
	  when "start"
			if line.include?("Layer height")
				layer_height = line.split(':').last.strip
				puts "Layer Height :: " + layer_height
		  	$phase = "find_layer_before_recovery_layer"
		  	puts "Finding layer before recovery layer to get initial Z height"
		  end

		when "find_layer_before_recovery_layer"
			if line.include?(";LAYER:" + ($layer.to_i - 1).to_s)
				puts "Found layer " + ($layer.to_i - 1).to_s
				$phase = "find_nonmesh_block"
				puts "Finding NONMESH block"
			end

		when "find_nonmesh_block"
			if line.include?("NONMESH")
				$phase="find_recovery_height"
				puts "Finding Recovery Height"
			end

		when "find_recovery_height"
			if line.include?("F300") && line.include?("Z")
				$recovery_height = line.split(' ').last.split('Z').last
				puts "Recovery Height :: " + $recovery_height
				puts "Finding first Extrusion Command"
				$phase = "find_recovery_layer"
			end

		when "find_recovery_layer"
		  if line.include?(";LAYER:" + $layer)
		  	$phase = "find_extrusion_value"
		  end
	  
	  when "find_extrusion_value"
	  	if line[0..1] == "G1"
	  		puts "First G1 :: " + line
	  		$first_extrusion = line.split(' ').last.strip
	  		puts "First Extrusion :: " + $first_extrusion
	  		break
			end
		end	  	
	end
end

def build_file file

	destination_file = $destination + 'recovered_' + file
	File.write(destination_file, ";RECOVERED\n")
	
	text=File.open($source + file).read
	text.gsub!(/\r\n?/, "\n")
	$phase = "start"

	puts "BUILDING RECOVERY FILE"
	puts destination_file
	puts "Copying Starter Initiation Code"
	count = 0
	text.each_line do |line|
		count = count + 1
		case $phase
		when "start"
			if line.include?("LAYER_COUNT")
				$phase = "add_headers"
				puts "Adding Customer extrusion setting, start height and starting fans"
			else
				case line[0]
				when ";"
					File.write(destination_file, line , mode: 'a')
				when "G"
					File.write(destination_file, ';REMOVING FOR RECOVERY ' + line, mode: 'a' )
				else
					File.write(destination_file, line , mode: 'a')
				end					
			end
		
		when "add_headers"
			File.write(destination_file, ";==============================================================\n", mode: 'a' )			
			File.write(destination_file, ";RECOVERY HEADER START=========================================\n", mode: 'a' )
			File.write(destination_file, "G92 " + $first_extrusion + " ;ADDING FOR RECOVERY\n", mode: 'a' )
			File.write(destination_file, "G1 Z10 F3000 ;Move Z up before reset" + "\n", mode: 'a' )
			File.write(destination_file, "G28 ;Home" + "\n", mode: 'a' )
			File.write(destination_file, "G1 Z" + $recovery_height + " F3000 ;Move Z up to recovery position" + "\n", mode: 'a' )
			File.write(destination_file, "M106 S100 ;Turn on fans\n", mode: 'a' )
			File.write(destination_file, ";RECOVERY HEADER END ==========================================\n", mode: 'a' )
			File.write(destination_file, ";==============================================================\n", mode: 'a' )			

			$phase = "find_recovery_layer"

		when "find_recovery_layer"
			if line.include?(";LAYER:" + $layer)
				File.write(destination_file, ";STARTING RECOVERY AT LAYER " + $layer + "\n", mode: 'a' )
				$phase = "copy"
				puts "Found at line :: " + count.to_s
				puts "Copying all gcode still to print"
			end

		when "copy"
			# Doing this line by line with ruby takes forever.. so let's be smarter.

			command = "cat " + $source + file + " | sed '1," + (count - 2).to_s + "d' >> " + destination_file
			puts command
			system(command)
			break
		
		end

	end

end

# If you don't pass a filename it takes the last file uploaded.
if ARGV[1] 
	file = ARGV[1]
else
	file = last_upload
end

puts "Original Gcode :: " + file
puts "Recover at layer :: " + $layer

# Have a look at the file and get any values we need.
analyse_file(file)

# Build a recover file
build_file(file)
