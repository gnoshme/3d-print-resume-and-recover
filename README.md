# README #

(Known Issues)
* Falls over on filenames with special characters



My 3d printer was on the blink recently and kept stalling during a print so I followed a tutorial on how to restart it by hacking the gcode file.  
After that, it made sense to create a script to do that more easily in the future.

### Environment ###
It's a ruby script.. I'm sorry it's not python but that's just the way it is. I'm a ruby guy.  Personally I use Octopi and I have an NTFS mount on it that I mount to from my desktop, 
so I'm running this on my desktop which is seeing files on the pi.  You can set it up however you want.

This assumes a linux environment including:
* ruby
* sed

### Configuration ###
This works for my setup.  To make it work for you:

* Edit the folder locations at the top of the file to point to where source gcode files are, and (an existing) directory where it should put the recovery files

### Running it ###
There are two ways to run this. Let's say you know your print failed on layer 42.  If you use Octopi you'll be able to see the layer number where it failed. If not, you've got to work it out somehow.

USE THE MOST RECENT FILE FROM THE UPLOAD FOLDER
3drecover 42

USE A SPECIFIC FILE IN THE UPLOAD FOLDER
3drecover 42 somefile.gcode

### Dangerous Assumptions! ###
In the tests I have done, everything at the top of the GCODE file that starts with a "G" should be ignored. 
There are other controls starting with an M that should be included.

THAT'S AS SOPHISTICATED AS THIS GETS RIGHT NOW.  If my logic is wrong, the resulting gcode will be wrong. Let's collaborate on this!

Other issues that come to mind:
* This assumes positive Z movement.. in other words, a printer where the extruder rises, not where the plate lowers.  I think the only place that is impacted here is the inital header line that is added to raise the extruder 10mm. 

### What it does ###
* It finds the layer you need to restart at, and skips back to the end of the previous layer gcode to find out what height it should go to.  This is the print layer height + the number of layers, but looking for it in the gcode feels "better".
* It remembers the file line number of the start of the recovery layer
* It scrapes the recovery line extruder setting (I don't really understand why - it's just what the manual recovery tutorials say to do)
* It skips through the gcode header data, accepting or rejecting each line.
* It adds recovery instructions to raise the extruder away from it's stalled position on the print, auto-homes, sets initial extruder setting, raises the Z axis to the recovery height
* Then for speed, it uses SED to copy all of the needed layer data to the recover file.  Just going through it line by line with ruby takes ages.